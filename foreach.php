<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>For each loop</title>
    </head>
    <body>
        <?php
        
        $run = array("Tamim"=> "85","Shakib"=>"100" , "Sabbir"=>"52" );
        
        foreach ($run as $name=>$run){
        echo "Batman=" .$name, " ,Run=" .$run;
        echo "<br>";
        }
        
        
        
         echo "<br>";
        
         
         $friends = [
             "John" =>["location"=>"dhaka", "age"=> 20, "profession"=> "designer"],
             "Mac" =>["location"=>"NY", "age"=> 30, "profession"=> "Programmer"],
             "Bell" =>["location"=>"San Francisco", "age"=> 22, "profession"=> "Team Leader"]
         ];
           
                 
         echo "<pre>";        
         foreach ($friends as $key=>$value){
             echo "name:". $key. "<br>";
             
             foreach ($value as $var=> $result){
                 
                 echo $var . ":".$result. "<br>";
             }
             echo "<br>";
             }
            
         echo "</pre>";
         
         
         echo "<br>";
         
         
         $friends = [
             "John" =>["location"=>"dhaka", "age"=> array(20,30,40), "profession"=> "designer"],
             "Mac" =>["location"=>"NY", "age"=> 34, "profession"=> "Programmer"],
             "Bell" =>["location"=>"San Francisco", "age"=> 22, "profession"=> "Team Leader"]
         ];
         
         
         echo "<pre>";  
         foreach ($friends as $name => $info){
             
             echo "name: ". $name. "<br>";
             
             foreach ($info as $key=> $var){
                 if(is_array($var)){
                     echo $key. ":<br>";
                     foreach ($var as $key => $age){
                         
                         echo $key. ":".$age . "<br>";
                     }
                 }
                 else 
                         {
                         echo $key. " : " . $var. "<br>";
                     }
                 }
                 echo "<br>";
                 
             }
             echo "<br>";
         
         echo "</pre>";
         
         
         
        ?>
    </body>
</html>
